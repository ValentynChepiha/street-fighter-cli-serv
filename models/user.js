exports.user = {
    id: { type: 'unique'},
    firstName: { required: true, mask: /^\S+$/, name: "First name" } ,
    lastName: { required: true, mask: /^\S+$/, name: "Last name" } ,
    email: { required: true, mask: /^[a-zA-Z]{1}\S+\@(gmail\.com)$/, name: "E-mail" } ,
    phoneNumber: { required: true, mask: /^(\+380){1}(\d{9})$/, name: "Phone number" } ,
    password: { required: true, mask: /\S{3,}$/, name: "Password" }
}
