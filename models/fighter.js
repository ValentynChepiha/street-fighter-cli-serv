exports.fighter = {
    "id": { type: 'unique'},
    "name": { required: true, minLength: 3, name: "Name" },
    "health": { default: 100 },
    "power": { required: true, minValue: 1, maxValue: 10, name: "Power" },
    "defense": { required: true, minValue: 1, maxValue: 10, name: "Defense" },
}
