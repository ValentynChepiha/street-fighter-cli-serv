const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/:id', ( req, res, next )=>{
    try {
      const { params } = req;
      res.data = FighterService.search( params );
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware);

router.get('/', ( req, res, next )=>{
    try {
      res.data = FighterService.getAll();
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware);

router.post('/', createFighterValid, ( req, res, next )=>{
    try {
      // todo ???
      if(!res.err){
        const { body } = req;
        res.data = FighterService.create( body );
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware);

router.put('/', updateFighterValid, ( req, res, next )=>{
    try {
      if(!res.err) {
        const { params: {id} } = req;
        const { dataArg } = res;
        res.data = FighterService.update(id, dataArg);
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware);

router.delete('/:id', ( req, res, next )=>{
    try {
      const { params: {id} } = req;
      res.data = FighterService.delete( id );
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware);


module.exports = router;