const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// + TODO: Implement route controllers for user
router.get('/:id', (req, res, next) => {
  try {
    const { params } = req;
    res.data = UserService.search( params );
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/', (req, res, next) => {
  try {
    res.data = UserService.getAll();
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
  try {
    // todo ???
    if(!res.err){
      const { body } = req;
      res.data = UserService.create( body );
    }
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
  try {
    if(!res.err) {
      const { params: {id} } = req;
      const { dataArg } = res;
      res.data = UserService.update(id, dataArg);
    }
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('', (req, res, next) => {
  try {
    const { params: {id} } = req;
    res.data = UserService.search( id );
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;