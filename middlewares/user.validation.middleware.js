const { user } = require('../models/user');
const ValidateService = require('../services/validateService');

const createUserValid = (req, res, next) => {
    // + TODO: Implement validatior for user entity during creation
    const userData = req.body || {} ;
    let message = '';

    Object.keys(user).forEach( key => {
        if(user[key].required){
            message += ValidateService.verifyEntity( userData[key], user[key] );
        } else if(userData[key]) {
            message += 'Error create user';
        }
    });

    // todo ???
    if( message ){
        // throw message;
        res.err = message;
    }
    next();
};

const updateUserValid = (req, res, next) => {
    // + TODO: Implement validatior for user entity during update
    const userData = req.body || {} ;
    const arg = {};
    let message = '';

    Object.keys(userData).forEach( key => {
        if(user[key].required){
            message += ValidateService.verifyEntity( userData[key], user[key] );
            arg[key] = userData[key];
        }  else {
            message += 'Error update user';
        }
    });

    res.dataArg = arg;

    if( message ){
        // throw message;
        res.err = message;
    }

    next();
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;