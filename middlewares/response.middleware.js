const responseMiddleware = (req, res, next) => {
   // + TODO: Implement middleware that returns result of the query

    const error = {
        error: true
    };

    const { data, err } = res;

    if( err ){
        error.message = err;
        res.status(400).send( JSON.stringify( error ));
    } else if(data && Object.keys(data).length){
        res.status(200).send( JSON.stringify(data) );
    } else {
        error.message = 'Not data found';
        res.status(404).send( JSON.stringify(error) );
    }

    next();
};

exports.responseMiddleware = responseMiddleware;