const { fighter } = require('../models/fighter');
const ValidateService = require('../services/validateService');

const createFighterValid = (req, res, next) => {
    // TODO: + Implement validatior for fighter entity during creation
    const fighterData = req.body || {} ;

    let message = '';

    Object.keys(fighter).forEach(key => {
        if(fighter[key].required){
            message += ValidateService.verifyEntity( fighterData[key], fighter[key] );
        } else if(fighterData[key]) {
            message += "Error create fighter";
        }
    });

    // todo ???
    if( message ){
        res.err = message;
    }
    next();
};

const updateFighterValid = (req, res, next) => {
    // TODO: + Implement validatior for fighter entity during update
    const fighterData = req.body || {} ;
    const arg = {};
    let message = '';

    Object.keys(fighterData).forEach(key => {
        if(fighter[key].required){
            message += ValidateService.verifyEntity( fighterData[key], fighter[key] );
            arg[key] = fighterData[key];
        } else {
            message += "Error update fighter";
        }
    });

    res.dataArg = arg;

    // todo ???
    if( message ){
        res.err = message;
    }
    next();
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;