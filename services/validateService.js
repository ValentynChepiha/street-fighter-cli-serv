class ValidateService {
    verifyMask( value, template ){
      return template.test(value);
    }

    numberLimits(value, min, max){
      return (value >= min && value <= max )
    }

    stringMinLength(value, minLength){
      return value.length >= minLength;
    }

    verifyEntity( value, template ){
      const errorText = `Invalid ${template.name}. `;

      if( !value){
        return errorText;
      }

      if(template.mask){
        return this.verifyMask( value, template.mask ) ? '' : errorText;
      }

      if(template.minLength){
        return this.stringMinLength( value, template.minLength) ? '' : errorText;
      }

      if(template.minValue && template.maxValue){
        return this.numberLimits( value, template.minValue, template.maxValue ) ? '' : errorText;
      }

      return '';
    }
}

module.exports = new ValidateService();