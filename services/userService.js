const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // + TODO: Implement methods to work with user

    getAll(){
        const users = UserRepository.getAll();
        if(!users) {
            return null;
        }
        return users;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    create(data){
        const { firstName, lastName, email, phoneNumber, password } = data;
        let item = UserRepository.getOne({ phoneNumber });
        if(item) {
            throw 'This phone number has already used';
        }

        item = UserRepository.getOne({ email });
        if(item) {
            throw 'This e-mail has already used';
        }

        item = UserRepository.getOne({ firstName, lastName, email, phoneNumber, password });
        if(item) {
            throw 'User already exists';
        }

        const result = UserRepository.create({ firstName, lastName, email, phoneNumber, password });
        if(!result){
            throw 'User not created';
        }
        return result;
    }

    update(id, data){
        let item = UserRepository.getOne({ id });
        if(!item) {
            throw 'User not found';
        }

        const result = UserRepository.update(id, data);
        if(!result){
            throw 'User not updated';
        }
        return result;
    }

    delete(id){
        const result = UserRepository.delete(id);
        if(!result){
            return null;
        }
        return result;
    }
}

module.exports = new UserService();