const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
  search(search) {
    const item = FighterRepository.getOne(search);
    if(!item) {
      return null;
    }
    return item;
  }

  getAll(){
    const users = FighterRepository.getAll();
    if(!users) {
      return null;
    }
    return users;
  }

  create(data){
    const { name, power, defense } = data;
    const item = FighterRepository.getOne({ name });
    if(item) {
      throw 'Fighter already exists';
    }
    const result = FighterRepository.create({ name, power, defense, health: 100 });
    if(!result){
      throw 'Fighter not created';
    }
    return result;
  }

  update(id, data){
    const item = FighterRepository.getOne({ id });
    if(!item) {
      throw 'Fighter not found';
    }
    const result = FighterRepository.update(id, data);
    if(!result){
      throw 'Fighter not updated';
    }
    return result;
  }

  delete(id){
    const result = FighterRepository.delete(id);
    if(!result){
      return null;
    }
    return result;
  }
}

module.exports = new FighterService();